# controller-fytopanel


* ![master:docker-build](https://gitlab.com/appf-anu/controller-fytopanel/badges/master/pipeline.svg?job=build_master) *master:docker-build*

* ![master:go-lint](https://gitlab.com/appf-anu/controller-fytopanel/badges/master/pipeline.svg?job=golint) *master:go-lint*

* ![master:go-staticcheck](https://gitlab.com/appf-anu/controller-fytopanel/badges/master/pipeline.svg?job=staticcheck) *master:go-staticcheck*

* ![master:go_vet](https://gitlab.com/appf-anu/controller-fytopanel/badges/master/pipeline.svg?job=go_vet) *master:go-vet*

* ![master:go_test](https://gitlab.com/appf-anu/controller-fytopanel/badges/master/pipeline.svg?job=go_test) *master:go-test*

* ![master:go_build](https://gitlab.com/appf-anu/controller-fytopanel/badges/master/pipeline.svg?job=go_build) *master:go-build*


control software for the PSI fytopanel lights



## usage
### options
```
--no-metrics: dont collect or send metrics to telegraf
--dummy: dont control the lights, only collect metrics
--conditions: conditions to use to run the lights
--scroll-mode: scroll all the channels sequentially
--disco-mode: randomly set channel values
--interval: what interval to run conditions/record metrics at, set to 0s to read 1 metric and exit. (default=10m)
--absolute: if your file is set in absolute light values (0-1022) rather than brightness percentages, set this flag
--host-tag: adds a host tag sent to telegraf
--group-tag: adds a group tag sent to telegraf
--did-tag: adds a did (data ID) tag sent to telgraf
```


examples
./controller-fytopanel --conditions conditions/testfile.csv /dev/ttyUSB0

./controller-fytopanel --no-metrics --conditions conditions/testfile.csv /dev/ttyUSB0

./controller-fytopanel --disco-mode /dev/ttyUSB0

./controller-fytopanel --scroll-mode /dev/ttyUSB0

### environment variables
CONDITIONS_FILE
NO_METRICS
DUMMY
ABSOLUTE
INTERVAL
NAME
HOST_TAG
DISCO
SCROLL

### docker
use the docker image by pulling from the appf repo

`docker pull registry.gitlab.com/appf-anu/controller-fytopanel`

create a docker container:

```docker create --name lights-conditions \
  -e TZ=Australia/Brisbane \
  -v /home/stormaes/conditions:/conditions \
  --network services-net \
registry.gitlab.com/appf-anu/controller-fytopanel
```

if you have a docker telegraf instance, it is helpful to add them to the same network, otherwise you can use the
environment variable "TELEGRAF_HOST" to specify the available telegraf host.

start the docker container

`docker start lights-conditions`


## RPi controller

There is now a raspberry pi controller. use the hypriotos flash script to flash a card.

`sudo ./flash -u user-data.yml --bootconf config.txt -d /dev/mmcblk0 https://github.com/hypriot/image-builder-rpi/releases/download/v1.12.3/hypriotos-rpi-v1.12.3.img.zip`

Flash SD card and then boot it connected to a display (no mouse/keyboard needed, but needs Ethernet with an internet connection to install packages). Wait until it says something like "Cloud-init finished ...".
