package main

import (
	"flag"
	"fmt"
	"github.com/jacobsa/go-serial/serial"
	ctools "gitlab.com/appf-anu/chamber-tools"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

/*
coolwhite	channel-1  # white
blue		channel-2  # blue
green		channel-3  # green
red		 	channel-4  # red
deepred		channel-5  #  deepred
farred		channel-6  # far red
infrared	channel-7  # infrared
cyan		channel-8  # NA
*/

const (
	adrtOne   = 0x01
	adrtGroup = 0x02
	adrtAll   = 0x0F
)

//for light channels
const (
	instrSetIntensity = 0
	//instrGetIntensity = 1
	instrSetTrigger = 2
	//instrGetTrigger   = 3
)

//for service channel 0 = absolute 2 - gid and err
const (
	//instrSetGid   = 0
	//instrGetGid   = 1
	//instrGetError = 2
	//for service channel 1 = absolute 9 - set speed
	instrSetSpeed = 3
	//for service channel 2 = absolute 15 - temp
	//instrGetTemp = 3
)

const (
	//serviceChannel0 = 2
	serviceChannel1 = 9 // all on
	//serviceChannel2 = 15
)

const (
	channel1 = 0
	channel2 = 1
	channel3 = 3
	channel4 = 4
	channel5 = 5
	channel6 = 6
	channel7 = 7
	channel8 = 8
)

var availableChannels = []int{
	channel1,
	channel2,
	channel3,
	channel4,
	channel5,
	channel6,
	channel7,
	channel8,
	//serviceChannel1, // all on
}

//const (
//	errnoLed = 0x01
//	errnoFet = 0x02
//	errnoEe  = 0x04
//	errnoExt = 0x10
//	errnoDac = 0x20
//	errnoCom = 0x80
//)

//const (
//	opcodeGetLightColor    = 40
//	opcodeGetVoltage       = 41
//	opcodeGetResistance    = 42
//	opcodeGetDacMap        = 43
//	opcodeGetSwitchMap     = 44
//	opcodeGetDeviceAddress = 61
//	opcodeGetDeviceFamily  = 62
//	opcodeGetChipTemp      = 63
//)

//const (
//	//versions of answer
//	//parse answer
//	cmdWithAns = 0
//	//get answer, but no need to parse
//	cmdDummyAns = 1
//	//no answer expected
//	cmdNoAns = 2
//)

var errLog *log.Logger

var (
	doTheDisco, doTheScroll, doTheCalibration bool
	allZero                                   bool
	noMetrics, dummy, absolute                bool
	conditionsPath                            string
	tags                                      ctools.TelegrafTags
	interval                                  time.Duration
	loopFirstDay                              bool
)

var port io.ReadWriteCloser

// ConstructPacket constructs a packet according to the protocol laid out by PSI to set a channel to value.
// address is the "address" of the light, a non-negative integer.
// lightChannel is the channel to set, 0,1,3,4,5,6,7,8. 2 has special meaning and so is excluded. 8 is all.
// instr is the type of op to do. 0 = set intensity, 2 = set trigger, 3 = ???
// value is the value to set within the packet, valid values are 0-1022.
// addrType is the packet type, for us this is always 1, leaving it here for future compatibility (if we actually find out
// how to do it.
func ConstructPacket(addrType, address, lightChannel, instr, value int) (packet [7]byte, err error) {
	if addrType != adrtOne && addrType != adrtGroup && addrType != adrtAll {
		err = fmt.Errorf("not valid packet address type")
		return
	}

	if addrType == adrtOne && (address == 0 || address >= 1000) {
		err = fmt.Errorf("address not valid for address type")
		return
	}

	if addrType == adrtGroup {
		address += 1000
	}

	// header always ST
	packet = [7]byte{'S', 'T', 0, 0, 0, 0, 0}
	// target + nibble of address
	packet[2] = byte((addrType << 4) | ((address >> 8) & 0x0F))
	// the rest of the address
	packet[3] = byte(address & 0xFF)

	// opcode + payload
	// opcode = 4b, channel + 2b instruction part
	// is 'instr' meant to be instr? G.
	// either way this should now be working... G.
	packet[4] = byte(((lightChannel << 4) & 0xF0) | ((instr << 2) & 0x0C) | ((value >> 8) & 0x03))
	packet[5] = byte(value & 0xFF)

	// checksum to get last byte

	for i := 0; i < 6; i++ {
		packet[6] ^= packet[i]
	}
	return
}

// CheckPacketLength checks the packet length and returns -1 of the packet length is invalid
func CheckPacketLength(answer []byte) int {
	answerLength := len(answer)
	if answerLength < 5 {
		return -1
	}

	//return packet - header UV - most of the times correct even for multiple devices
	//might be some junk at the begining, try to find UV position
	var pos int
	for pos = 0; pos < answerLength-5; pos++ {
		if answer[pos] == 'U' && answer[pos+1] == 'V' {
			break
		}
	}
	//UV nenalezeno?
	if pos == answerLength-5 {
		return -1
	}
	return pos
}

//------./main.go:216:15: (packet[2] & 0x0F) (8 bits) too small for shift of 8
//./main.go:220:9: (packet[4] & 0x03) (8 bits) too small for shift of 8---------------------------------------------------------------------
//bool TFytoCtrl::ParsePacket(uint8_t *answer, int32_t len, uint16_t *res, uint32_t address, int32_t iopcode)
//func parsePacket(answer []byte, iopCode byte) (rType, rAddress, ropCode, res byte, err error) {
//	pos := CheckPacketLength(answer)
//	if pos < 0 {
//		err = fmt.Errorf("neg pos for packt")
//		return
//	}
//
//	//CRC packetu
//	crc := byte(0)
//	for i := pos; i < pos+7; i++ {
//		crc ^= answer[i]
//	}
//
//	//chyba v CRC
//	if crc != 0 {
//		err = fmt.Errorf("CRC no match")
//		return
//	}
//
//	packet := answer[pos:]
//
//	//parsovani adresy a prijemce
//	rType = (packet[2] >> 4)
//	rAddress = (((packet[2] & 0x0F) << 8) | packet[3])
//
//	//parsovani kodu operace a payloadu
//	ropCode = (packet[4] >> 2)
//	res = ((packet[4] & 0x03) << 8) | packet[5]
//
//	if rType == adrtOne && rAddress == 0 && ropCode == iopCode {
//		return
//	}
//	err = fmt.Errorf("Everything is not alright: %x %x %x %x",
//		rType,
//		rAddress,
//		ropCode,
//		res)
//	return
//}

func activatePacket(lightChannel int) (packet []byte, err error) {
	// for some reason value needs to be a non-negative non-zero integer for these packets to correctly activate lights
	part1, err := ConstructPacket(adrtAll, 0, lightChannel, instrSetTrigger, 1) // set trigger on
	if err != nil {
		return
	}
	part2, err := ConstructPacket(adrtAll, 0, lightChannel, instrSetSpeed, 1) // ???
	if err != nil {
		return
	}
	// spread em
	packet = append(part1[:], part2[:]...)
	return
}

func deActivatePacket(lightChannel int) (packet []byte, err error) {

	part1, err := ConstructPacket(adrtAll, 0, lightChannel, instrSetTrigger, 0) // set trigger off
	if err != nil {
		return
	}
	part2, err := ConstructPacket(adrtAll, 0, lightChannel, instrSetSpeed, 0) // ???
	if err != nil {
		return
	}
	// spread em
	packet = append(part1[:], part2[:]...)
	return
}

func setIntensityPacket(lightChannel, value int) (packet []byte, err error) {
	pack, err := ConstructPacket(adrtAll, 0, lightChannel, instrSetIntensity, value) // set trigger on
	packet = pack[:]
	return
}

func random(max int) int {
	return rand.Intn(max)
}

func setOne(port io.ReadWriteCloser, lightChannel, value int) (err error) {
	if value == ctools.NullTargetInt || value < 0 {
		return nil
	}
	intensityPackt, err := setIntensityPacket(lightChannel, value)
	if err != nil {
		errLog.Println(err)
		return
	}
	activatePackt, err := activatePacket(lightChannel)
	if err != nil {
		errLog.Println(err)
		return
	}

	port.Write(intensityPackt)
	port.Write(activatePackt)
	return nil
}

func setMany(port io.ReadWriteCloser, values []int) (err error) {
	for i := 0; i < len(values) && i < len(availableChannels); i++ {
		lightChannel := availableChannels[i]
		value := values[i]
		if value == ctools.NullTargetInt || value < 0 {
			continue // dont set null values or values less than 0.
		}
		intensityPackt, err := setIntensityPacket(lightChannel, value)

		if err != nil {
			errLog.Println(err)
			return err
		}

		activatePackt, err := activatePacket(lightChannel)
		if err != nil {
			errLog.Println(err)
			return err
		}

		port.Write(intensityPackt)
		port.Write(activatePackt)
	}

	return nil
}

func scrollMode(max int) {
	if max > 1022 {
		max = 1022
	}
	setAllZero()

	scrollOne := func(channelIndex int, lightChannel int) {
		fmt.Printf("Scrolling channel-%d #%d\n", channelIndex, lightChannel)
		for value := -math.Pi / 2; value <= 1.5*math.Pi; value += 0.1 {
			sinV := (1 + math.Sin(value)) / 2
			thisV := sinV * float64(max)
			fmt.Println(int(math.Round(thisV)))
			setOne(port, lightChannel, int(math.Round(thisV)))
			time.Sleep(100 * time.Millisecond)
		}
	}

	for {
		for index, lightChannel := range availableChannels {
			scrollOne(index, lightChannel)
		}
		scrollOne(-1, serviceChannel1)
		time.Sleep(1 * time.Second)
	}
}

func calibrateMode(max int) {
	channelNames := []string{
		"CoolWhite",
		"Blue",
		"Green",
		"Red",
		"DeepRed",
		"FarRed",
		"InfraRed",
		"Cyan",
	}

	if max > 1022 {
		max = 1022
	}
	for _, lightChannel := range availableChannels {
		setOne(port, lightChannel, 0)
	}

	for {
		for i, lightChannel := range availableChannels {
			fmt.Printf("%s\t\tchannel-%d #%d -> %d\n",
				channelNames[i],
				i+1,
				lightChannel,
				max,
			)
			setOne(port, lightChannel, max)
			time.Sleep(20 * time.Second)
			setOne(port, lightChannel, 0)
			time.Sleep(5*time.Second)
		}
	}
}

func setAllRandom(port io.ReadWriteCloser, max int) {
	if max > 1022 {
		max = 1022
	}
	// skip the first channel, which is cool white
	for i := 1; i < len(availableChannels); i++ {
		lightChannel := availableChannels[i]
		value := random(max)
		intensityPackt, err := setIntensityPacket(lightChannel, value)
		if err != nil {
			errLog.Println(err)
		}
		activatePackt, err := activatePacket(lightChannel)
		if err != nil {
			errLog.Println(err)
		}
		port.Write(intensityPackt)
		port.Write(activatePackt)
	}
}

type lightStatus struct {
	CoolWhiteTarget float64
	BlueTarget      float64
	GreenTarget     float64
	RedTarget       float64
	DeepRedTarget   float64
	FarRedTarget    float64
	InfraRedTarget  float64
	CyanTarget      float64
}

func newLightStatusPtr() *lightStatus {
	ls := &lightStatus{
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
	}
	return ls
}

// runStuff, should send values and write metrics.
// returns true if program should continue, false if program should retry
func runStuff(point *ctools.TimePoint) bool {
	/*
	   coolwhite	channel-1  # white
	   blue			channel-2  # blue
	   green		channel-3  # green
	   red		 	channel-4  # red
	   deepred		channel-5  #  deepred
	   farred		channel-6  # far red
	   infrared		channel-7  # infrared
	   cyan			channel-8  # NA
	*/

	// setup multiplier
	multiplier := 1.0
	if !absolute {
		multiplier = 10.22
	}
	// collect the values that are meaningful to us
	theValues := []float64{
		point.CoolWhite,
		point.Blue,
		point.Green,
		point.Red,
		point.DeepRed,
		point.FarRed,
		point.InfraRed,
		point.Cyan,
	}
	// convert them to int
	intVals := make([]int, len(theValues))
	for i, v := range theValues {
		if v == ctools.NullTargetFloat64 || v < 0 {
			intVals[i] = ctools.NullTargetInt
			continue
		}
		intVals[i] = ctools.Clamp(int(v*multiplier), 0, 1022)
	}

	if err := setMany(port, intVals); err != nil {
		errLog.Println(err)
		return false
	}

	status := newLightStatusPtr()
	// set targets
	status.CoolWhiteTarget = (point.CoolWhite * multiplier) / 10.22
	status.BlueTarget = (point.Blue * multiplier) / 10.22
	status.GreenTarget = (point.Green * multiplier) / 10.22
	status.RedTarget = (point.Red * multiplier) / 10.22
	status.DeepRedTarget = (point.DeepRed * multiplier) / 10.22
	status.FarRedTarget = (point.FarRed * multiplier) / 10.22
	status.InfraRedTarget = (point.InfraRed * multiplier) / 10.22
	status.CyanTarget = (point.Cyan * multiplier) / 10.22

	// success
	errLog.Printf("ran %s %+v", point.Datetime.Format(time.RFC3339), intVals)
	for x := 0; x < 5; x++ {
		if err := ctools.WriteMetricUDP("fytopanel2", tags, status); err != nil {
			errLog.Println(err)
			time.Sleep(200 * time.Millisecond)
			continue
		}
		break
	}

	return true
}

func setAllZero() {
	activatePackt, err := deActivatePacket(serviceChannel1)
	if err != nil {
		errLog.Println(err)
	}
	port.Write(activatePackt)
}

func discoMode(max int) {
	ticker := time.NewTicker(500 * time.Millisecond)
	for range ticker.C {
		setAllRandom(port, max)
	}
}

func init() {
	var err error
	errLog = log.New(os.Stderr, "[fytopanel] ", log.Ldate|log.Ltime|log.Lshortfile)
	// get the local zone and offset

	flag.BoolVar(&doTheDisco, "disco", false, "turn on disco mode")
	if tempV := strings.ToLower(os.Getenv("DISCO")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			doTheDisco = true
		} else {
			doTheDisco = false
		}
	}
	flag.BoolVar(&doTheScroll, "scroll", false, "turn on scroll mode")
	if tempV := strings.ToLower(os.Getenv("SCROLL")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			doTheScroll = true
		} else {
			doTheScroll = false
		}
	}

	flag.BoolVar(&doTheCalibration, "calibrate", false, "turn on disco mode")
	if tempV := strings.ToLower(os.Getenv("CALIBRATE")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			doTheCalibration = true
		} else {
			doTheCalibration = false
		}
	}

	flag.BoolVar(&allZero, "off", false, "turn off all channels")

	hostname := os.Getenv("NAME")

	flag.BoolVar(&noMetrics, "no-metrics", false, "dont collect metrics")
	if tempV := strings.ToLower(os.Getenv("NO_METRICS")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			noMetrics = true
		} else {
			noMetrics = false
		}
	}
	flag.BoolVar(&dummy, "dummy", false, "dont send conditions to light")
	if tempV := strings.ToLower(os.Getenv("DUMMY")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			dummy = true
		} else {
			dummy = false
		}
	}
	flag.BoolVar(&absolute, "absolute", false, "use absolute light values in conditions file, not percentages")
	if tempV := strings.ToLower(os.Getenv("ABSOLUTE")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			absolute = true
		} else {
			absolute = false
		}
	}

	flag.BoolVar(&loopFirstDay, "loop", false, "loop over the first day")
	if tempV := strings.ToLower(os.Getenv("LOOP")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			loopFirstDay = true
		} else {
			loopFirstDay = false
		}
	}

	flag.StringVar(&tags.Host, "host-tag", hostname, "host tag to add to the measurements")
	if tempV := os.Getenv("HOST_TAG"); tempV != "" {
		tags.Host = tempV
	}

	flag.StringVar(&tags.Group, "group-tag", "nonspc", "host tag to add to the measurements")
	if tempV := os.Getenv("GROUP_TAG"); tempV != "" {
		tags.Group = tempV
	}

	flag.StringVar(&tags.DataID, "did-tag", "", "data id tag")
	if tempV := os.Getenv("DID_TAG"); tempV != "" {
		tags.DataID = tempV
	}

	flag.StringVar(&conditionsPath, "conditions", "", "conditions file to run")
	if tempV := os.Getenv("CONDITIONS_FILE"); tempV != "" {
		conditionsPath = tempV
	}

	flag.DurationVar(&interval, "interval", time.Minute*10, "interval to run conditions/record metrics at")
	if tempV := os.Getenv("INTERVAL"); tempV != "" {
		interval, err = time.ParseDuration(tempV)
		if err != nil {
			errLog.Println("Couldn't parse interval from environment")
			errLog.Println(err)
		}
	}
	flag.Parse()
	if noMetrics && dummy {
		errLog.Println("dummy and no-metrics specified, nothing to do.")
		os.Exit(1)
	}

	errLog.Printf("timezone: \t%s\n", ctools.ZoneName)
	errLog.Printf("tags: %+v", tags)
	errLog.Printf("file: \t%s\n", conditionsPath)
	errLog.Printf("interval: \t%s\n", interval)

}

func main() {
	gracefulStop := make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	// Set up options.
	// self.ser = serial.Serial('/dev/ttyUSB{}'.format(x), 9600, bytesize=serial.EIGHTBITS,
	// parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
	// rtscts=False, dsrdtr=False, xonxoff=False)
	var err error

	portName := flag.Arg(0)
	fi, err := os.Stat(portName)
	if os.IsNotExist(err) {
		errLog.Printf("%s does not exist, trying /dev/ttyUSB0", portName)
		portName = "/dev/ttyUSB0"
		fi, err = os.Stat(portName)
		if os.IsNotExist(err) {
			errLog.Fatalf("%s does not exist. exiting due to %v", portName, err)
		}
	}


	switch mode := fi.Mode(); {
	case mode&os.ModeSymlink != 0:
		fmt.Printf("%s is a symbolic link", portName)
	case mode&os.ModeDevice != 0:
		errLog.Printf("%s is a device", portName)
	}

	//9600 baud, 8 bits, no parity, handshake off, free mode
	options := serial.OpenOptions{
		PortName:          portName,
		BaudRate:          9600,
		DataBits:          8,
		StopBits:          1,
		ParityMode:        serial.PARITY_NONE,
		MinimumReadSize:   4,
		RTSCTSFlowControl: true,
	}
	port, err = serial.Open(options)
	if err != nil {
		panic(err)
	}
	go func() {
		sig := <-gracefulStop
		fmt.Printf("caught sig: %+v", sig)
		os.Exit(0)
	}()
	defer port.Close()
	if allZero {
		setAllZero()
	}
	if doTheDisco {
		discoMode(1022)
	}
	if doTheScroll {
		scrollMode(1022)
	}
	if doTheCalibration {
		calibrateMode(1022)
	}

	if !dummy && conditionsPath != "" {
		go ctools.RunConditions(errLog, runStuff, conditionsPath, loopFirstDay)
		select {}
	}
}
